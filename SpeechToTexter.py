import io
import os

import json
import credentials
import worderror
import asr_evaluation
import json

from os.path import join, dirname
from tkinter import *
from tkinter.filedialog import askopenfilename
from tkinter.messagebox import showerror

# Import IBM specific libraries
from watson_developer_cloud import SpeechToTextV1

# Import Google specific libraries
from google.cloud import speech
from google.cloud.speech import enums
from google.cloud.speech import types
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = credentials.googlecredential

# Import speech_recognition module for Microsoft Azure
import speech_recognition as sr
from os import path

# Global Variables
ibmresultuk = ''
ibmresultus = ''
googleresult = ''
microsoftresult = ''


class IBMRecogniser:  # IBM's Speech To Text
    def __init__(self):
        # Read in credentials to use for calling the IBM services.
        self.speech_to_text = SpeechToTextV1(
            username=credentials.IBMusername,
            password=credentials.IBMpassword,
            x_watson_learning_opt_out=False
        )

    # Take the filepath of an audio file and perform recognition, print the result to text file "IBMResult.txt".
    def RecogniseAndWriteResult(self, filepath, local):
        self.speech_to_text.models()
        if local == 'uk':
            self.speech_to_text.get_model('en-UK_BroadbandModel')
        elif local == 'us':
            self.speech_to_text.get_model('en-US_BroadbandModel')
        with open(join(dirname(__file__), filepath), 'rb') as audio_file:
            returned = self.speech_to_text.recognize(audio_file, content_type='audio/wav', timestamps=True, word_confidence=True)
            #print(json.dumps(returned))

        if local == 'uk':
            global ibmresultuk

            for result in returned["results"]:
                ibmresultuk += result["alternatives"][0]["transcript"]

            text_file = open("IBMResult-UK.txt", "w")
            text_file.write(ibmresultuk)
            text_file.close()
        elif local == 'us':
            global ibmresultus

            for result in returned["results"]:
                ibmresultus += result["alternatives"][0]["transcript"]

            text_file = open("IBMResult-US.txt", "w")
            text_file.write(ibmresultus)
            text_file.close()


class GoogleRecogniser:  # Google's Speech To Text
    def __init__(self):
        self.client = speech.SpeechClient()

    def RecogniseAndWriteResult(self, filepath):
        # Loads the audio into memory
        with io.open(join(dirname(__file__), filepath), 'rb') as audio_file:
            content = audio_file.read()
            audio = types.RecognitionAudio(content=content)

        config = types.RecognitionConfig(
            encoding=enums.RecognitionConfig.AudioEncoding.LINEAR16,
            sample_rate_hertz=44100,
            language_code='en-AU')

        # Detects speech in the audio file
        response = self.client.recognize(config, audio)
        #print(json.dumps(response))
        
        global googleresult

        for result in response.results:
            googleresult += result.alternatives.pop(0).transcript

        # Writes the result to a file
        text_file = open("GoogleResult.txt", "w")
        text_file.write(googleresult)
        text_file.close()


class AzureRecogniser:  # Microsoft's Speech To Text
    def __init__(self):
        self.r = sr.Recognizer()
        self.BING_KEY = credentials.bingkey

    def RecogniseAndWriteResult(self, filepath):
        with sr.AudioFile(path.join(path.dirname(path.realpath(__file__)), filepath)) as source:
            audio = self.r.record(source)

        global microsoftresult

        try:
            microsoftresult = self.r.recognize_bing(audio, key=self.BING_KEY, language="en-US", show_all=False)
            # print(json.dumps(microsoftresult))
        except sr.UnknownValueError:
            print("Microsoft Bing Voice Recognition could not understand audio")
        except sr.RequestError as e:
            print("Could not request results from Microsoft Bing Voice Recognition service; {0}".format(e))

        # Writes the result to a file
        text_file = open("MicrosoftResult.txt", "w")
        text_file.write(microsoftresult)
        text_file.close()

# ibm = IBMRecogniser()
# ibm.RecogniseAndWriteResult('Edited Audio/Oral Histories 01-01-mono.wav', 'uk')

# ibm = IBMRecogniser()
# ibm.RecogniseAndWriteResult('Edited Audio/Oral Histories 01-01-mono.wav', 'us')

# google = GoogleRecogniser()
# google.RecogniseAndWriteResult('Edited Audio/Oral Histories 01-01-mono.wav')

# azure = AzureRecogniser()
# azure.RecogniseAndWriteResult('Edited Audio/Oral Histories 01-01.wav')


# GUI For testing, TODO.

class Window(Frame):

    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.master = master
        self.init_window()
        self.transcript = ''
        self.audiostereo = ''
        self.audiomono = ''

    # Creation of init_window
    def init_window(self):
        self.master.title("Speech To Text Testing")
        self.pack(fill=BOTH, expand=1)

        select1 = Button(self, text="Select Accurate Transcript", command=self.load_text, width=25)
        select1.place(x=5, y=30)

        select2 = Button(self, text="Select Stereo WAV File", command=lambda: self.load_audio('stereo'), width=25)
        select2.place(x=5, y=60)

        select3 = Button(self, text="Select Mono WAV File", command=lambda: self.load_audio('mono'), width=25)
        select3.place(x=5, y=90)

        start = Button(self, text="Start Test", command=self.testasr, width=25)
        start.place(x=100, y=140)

        start = Button(self, text="Secondary WER Test", command=self.testwer, width=25)
        start.place(x=100, y=170)

        googlewer = Label(self, text="Google WER: ")
        googlewer.place(x=250, y=25)

        azurewer = Label(self, text="Azure WER: ")
        azurewer.place(x=250, y=45)

        ibmuswer = Label(self, text="IBM-US WER: ")
        ibmuswer.place(x=250, y=65)

        # ibmukwer = Label(self, text="IBM-UK WER: ")
        # ibmukwer.place(x=250, y=85)

    def load_text(self):
        fname = askopenfilename(filetypes=(("Text Files", "*.txt"),
                                           ("All files", "*.*")))

        if fname:
            try:
                print("""Loaded transcript file""")
                print(fname)
                self.transcript = fname
            except:  # <- naked except is a bad idea
                showerror("Open Source File", "Failed to read file\n'%s'" % fname)
            return

    def load_audio(self, type):
        fname = askopenfilename(filetypes=(("WAV Files", "*.wav"),
                                           ("All files", "*.*")))

        if fname:
            try:
                if type == 'stereo':
                    self.audiostereo = fname
                    print("""Loaded stereo file""")
                elif type == 'mono':
                    self.audiomono = fname
                    print("""Loaded mono file""")
                else:
                    print("load_audio argument must be either \'stereo\' or \'mono\'")
                print(fname)
            except:  # <- naked except is a bad idea
                showerror("Open Source File", "Failed to read file\n'%s'" % fname)
            return

    def testwer(self):
        print("Google:")
        print(os.system("wer \""+self.transcript+"\" \"GoogleResult.txt\""))
        print("Microsoft:")
        print(os.system("wer \"" + self.transcript + "\" \"MicrosoftResult.txt\""))
        print("IBM-US:")
        print(os.system("wer \""+self.transcript+"\" \"IBMResult-US.txt\""))
        # print("IBM-UK:")
        # print(os.system("wer \""+self.transcript+"\" \"IBMResult-UK.txt\""))

    def testasr(self):
        if self.transcript == '' or self.audiostereo == '' or self.audiomono == '':
            showerror("Files Missing", "One or more files have not been selected, please ensure that the transcript, "
                                       "stereo wav file and mono wav file have all been selected using the buttons"
                                       " above.")
            return

        # Read in transcript and remove punctation prior to WER calculation.
        transfile = open(self.transcript, 'r')
        transtring = transfile.read()
        transtringnopunc = transtring.replace(',', '')
        transtringnopunc = transtringnopunc.replace('.', '')
        transtringnopunc = transtringnopunc.replace('!', '')

        global ibmresultuk
        global ibmresultus
        global googleresult
        global microsoftresult

        ibm = IBMRecogniser()

        # # IBM-UK ASR
        # ibm.RecogniseAndWriteResult(self.audiostereo, 'uk')
        #
        # # Remove punctuation from results
        # ibmresultuknopunc = ibmresultuk.replace(',', '')
        # ibmresultuknopunc = ibmresultuknopunc.replace('.', '')
        # ibmresultuknopunc = ibmresultuknopunc.replace('!', '')
        #
        # # Calculate WER
        # ibmukwervar = worderror.wer(transtringnopunc.split(), ibmresultuknopunc.split())
        # print("IBM UK WER: "+ str(ibmukwervar))
        # ibmukwervarlabel = Label(self, text=str(ibmukwervar))
        # ibmukwervarlabel.place(x=350, y=85)

        # IBM-US ASR
        ibm.RecogniseAndWriteResult(self.audiostereo, 'us')

        # Remove punctuation from results
        ibmresultusnopunc = ibmresultus.replace(',', '')
        ibmresultusnopunc = ibmresultusnopunc.replace('.', '')
        ibmresultusnopunc = ibmresultusnopunc.replace('%HESITATION', '')
        ibmresultusnopunc = ibmresultusnopunc.replace('!', '')

        # Calculate WER
        ibmuswervar = worderror.wer(transtringnopunc.split(), ibmresultusnopunc.split())
        print("IBM US WER: " + str(ibmuswervar))
        ibmuswervarlabel = Label(self, text=str(ibmuswervar))
        ibmuswervarlabel.place(x=350, y=65)

        # Google ASR
        google = GoogleRecogniser()
        google.RecogniseAndWriteResult(self.audiomono)

        # Remove punctuation from results
        googlenopunc = googleresult.replace(',', '')
        googlenopunc = googlenopunc.replace('.', '')
        googlenopunc = googlenopunc.replace('!', '')

        # Calculate WER
        googlewervar = worderror.wer(transtringnopunc.split(), googlenopunc.split())
        print("Google WER: " + str(googlewervar))
        googlewervarlabel = Label(self, text=str(googlewervar))
        googlewervarlabel.place(x=350, y=25)

        # Microsoft ASR
        azure = AzureRecogniser()
        azure.RecogniseAndWriteResult(self.audiostereo)

        # Remove punctuation from results
        microsoftnopunc = microsoftresult.replace(',', '')
        microsoftnopunc = microsoftnopunc.replace('.', '')
        microsoftnopunc = microsoftnopunc.replace('!', '')

        # Calculate WER
        microsoftwervar = worderror.wer(transtringnopunc.split(), microsoftnopunc.split())
        print("Azure WER: " + str(microsoftwervar))
        microsoftwervarlabel = Label(self, text=str(microsoftwervar))
        microsoftwervarlabel.place(x=350, y=45)
        
        ibmresultuk = ''
        ibmresultus = ''
        googleresult = ''
        microsoftresult = ''

if __name__ == '__main__':
    root = Tk()
    root.geometry("400x250")
    prog = Window(root)
    root.mainloop()
