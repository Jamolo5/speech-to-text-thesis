# README #

### What is this repository for? ###

This repository hosts the code used for James Hercock's final year research project for completion of a Master of Professional Engineering (Software).
You may use this code to run word error rate tests on IBM, Microsoft, and Google speech to text cloud services.

### How do I get set up? ###

To run this code, simply download the repository, install the required python modules in the requirements.txt file.

	pip install -r requirements.txt
	
You should also edit the file credentials.py to contain your IBM username and password, contain the path to your google cloud credential json file, and your bing speech key.

Then, simply run the code using the command 

	python SpeechToTexter.py

which will open the self-explanatory user interface.